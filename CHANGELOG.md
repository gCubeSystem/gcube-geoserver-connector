# Changelog for session checker (deprecated)

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v0.1.0-SNAPSHOT] - 2023-06-13

- ported to git
